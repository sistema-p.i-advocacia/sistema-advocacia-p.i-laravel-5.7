<?php

use Illuminate\Database\Seeder;

class AreaAtuacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('area_atuacaos')->insert([
            ['nome'  => 'Direito Civil'],
            ['nome'  => 'Direito Comercial'],
            ['nome'  => 'Direito do Consumidor'],
            ['nome'  => 'Direito Contratual'],
            ['nome'  => 'Direito Penal'],
            ['nome'  => 'Direito Trabalhista'],
            ['nome'  => 'Direito Tributário'],
        ]);
    }
}
