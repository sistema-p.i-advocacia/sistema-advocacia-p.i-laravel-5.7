<?php

use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            ['nome'  => 'Fernando', 'email' => 'Fernando@gmail.com',
             'telefone1' => '99999-9999', 'telefone2' => '99999-9999'],
            ['nome'  => 'kelvin', 'email' => 'kelvin@gmail.com',
             'telefone1' => '99999-9999', 'telefone2' => '99999-9999'],
            ['nome'  => 'Diego', 'email' => 'Diego@gmail.com',
             'telefone1' => '99999-9999', 'telefone2' => '99999-9999'],
        ]);
    }
}
