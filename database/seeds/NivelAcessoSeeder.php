<?php

use Illuminate\Database\Seeder;

class NivelAcessoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nivel_acessos')->insert([
            ['nome'  => 'admin'],
            ['nome'  => 'funcionario'],
        ]);
    }
}
