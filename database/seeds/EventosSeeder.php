<?php

use Illuminate\Database\Seeder;

class EventosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_eventos')->insert([
            ['nome'  => 'Reunião', 'cor'  => '#7ee840'],
            ['nome'  => 'Almoço', 'cor'  => '##ff0000']



        ]);
    }
}
