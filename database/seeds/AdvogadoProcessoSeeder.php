<?php

use Illuminate\Database\Seeder;

class AdvogadoProcessoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advogado_processos')->insert([
            ['advogado_id'  => 1, 'processo_id' => 1]
        ]);
    }
}
