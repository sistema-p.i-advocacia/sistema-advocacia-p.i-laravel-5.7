<?php

use Illuminate\Database\Seeder;

class PessoaJuridicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pessoa_juridicas')->insert([
            ['cnpj'  => '654.654.654-65', 'data_fundacao' => '1998-01-26',
              'cliente_id' => 2 ]
        ]);
    }
}
