<?php

use Illuminate\Database\Seeder;

class AdvogadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advogados')->insert([
            ['advogados.nome'=>'Cristima', 'advogados.registro_oab'=>123456
            ,'advogados.email'=>'Cristima@gmail.com', 'advogados.telefone'=>'3333-6666'
            , 'advogados.nivel_acesso_id'=>1],
            ['advogados.nome'=>'Fernanda', 'advogados.registro_oab'=>123456
            ,'advogados.email'=>'Fernanda@gmail.com', 'advogados.telefone'=>'3333-6666'
            , 'advogados.nivel_acesso_id'=>1],
            ['advogados.nome'=>'Luiz', 'advogados.registro_oab'=>123456
            ,'advogados.email'=>'Luiz@gmail.com', 'advogados.telefone'=>'3333-6666'
            , 'advogados.nivel_acesso_id'=>1],
            ['advogados.nome'=>'Pedro', 'advogados.registro_oab'=>123456
            ,'advogados.email'=>'Pedro@gmail.com', 'advogados.telefone'=>'3333-6666'
            , 'advogados.nivel_acesso_id'=>1],
        ]);
    }
}
