<?php

use Illuminate\Database\Seeder;

class HistoricoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('historicos')->insert([
            ['data_publicacao'  => '2019-01-26', 'descricao' => 'Por provas suficientes Zé foi preso',
            'anexo' => 'Vazio', 'processo_id' => 1]
        ]);
    }
}
