<?php

use Illuminate\Database\Seeder;

class PessoaFisicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pessoa_fisicas')->insert([
            ['cpf'  => '654.654.654-65', 'data_nascimento' => '1998-01-26',
             'estado_civil' => 'Casado', 'escolaridade' => '3° Completo',
             'profissao' => 'Programador', 'sexo' => 'M', 'etnia' => 'Parda',
             'nome_mae' => 'maria', 'nome_pai' => 'zé', 'cliente_id' => 1 ]
        ]);
    }
}
