<?php

use Illuminate\Database\Seeder;

class VaraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('varas')->insert([
            ['nome'  => '1ª VARA CÍVEL DE BRASÍLIA', 'natureza_da_vara' => 'Cívil',
              'endereco' => 'Brasília, Asa Sul: quadra 709 lote 3' ],
            ['nome'  => 'Varas da Infância e da Juventude ', 'natureza_da_vara' => 'Infrator',
              'endereco' => 'Brasília, Asa Norte: quadra 201 lote 666' ],
        ]);
    }
}
