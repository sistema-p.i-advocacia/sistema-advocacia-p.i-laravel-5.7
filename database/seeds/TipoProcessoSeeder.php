<?php

use Illuminate\Database\Seeder;

class TipoProcessoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_processos')->insert([
            ['nome'  => 'feminicidio'],
            ['nome'  => 'Abuso de autoridade'],
            ['nome'  => 'Racismo'],
            ['nome'  => 'Bullying'],
            ['nome'  => 'Terrorismo'],
        ]);
    }
}
