<?php

use Illuminate\Database\Seeder;

class EventoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('eventos')->insert([
            ['nome'=>'Evento Teste-1', 'data_inicio'=>'2019-05-11', 'data_termino'=>'2019-05-12', 'tipo_evento_id'=>2],
            ['nome'=>'Evento Teste-2', 'data_inicio'=>'2019-05-11', 'data_termino'=>'2019-05-13', 'tipo_evento_id'=>1],
            ['nome'=>'Evento Teste-3', 'data_inicio'=>'2019-05-14', 'data_termino'=>'2019-05-14', 'tipo_evento_id'=>2],
            ['nome'=>'Evento Teste-3', 'data_inicio'=>'2019-05-17', 'data_termino'=>'2019-05-19', 'tipo_evento_id'=>1],


        ]);
    }
}
