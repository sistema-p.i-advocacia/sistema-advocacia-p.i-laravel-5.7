<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
            NivelAcessoSeeder::class,
            AdvogadoSeeder::class,
            ClienteSeeder::class,
            PessoaFisicaSeeder::class,
            PessoaJuridicaSeeder::class,
            VaraSeeder::class,
            StatusSeeder::class,
            TipoProcessoSeeder::class,
            EventosSeeder::class,
            ProcessoSeeder::class,
            AdvogadoProcessoSeeder::class,
            AreaAtuacaoSeeder::class,
            AdvogadoAreaAtuacaoSeeder::class,
            HistoricoSeeder::class,
            EventoSeeder::class,
        ]);
    }
}
