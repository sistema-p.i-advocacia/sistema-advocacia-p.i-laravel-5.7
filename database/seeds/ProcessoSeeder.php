<?php

use Illuminate\Database\Seeder;

class ProcessoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('processos')->insert([
            ['valor_causa'  => '12,200,00', 
            'assunto' => 'Mataram Ana',
            'reu' => 'CULPADASSO',
            'numero_processo' => 12, 
            'cliente_id' => 1, 
            'tipo_processo_id' => 1,
            'vara_id' => 1, 
            'statu_id' => 1],
            ['valor_causa'  => '12,200,00', 
            'assunto' => 'Mataram Ana',
            'reu' => 'CULPADASSO',
            'numero_processo' => 12, 
            'cliente_id' => 1, 
            'tipo_processo_id' => 1,
            'vara_id' => 1, 
            'statu_id' => 2],
            ['valor_causa'  => '12,200,00', 
            'assunto' => 'Mataram Ana',
            'reu' => 'CULPADASSO',
            'numero_processo' => 12, 
            'cliente_id' => 1, 
            'tipo_processo_id' => 1,
            'vara_id' => 1, 
            'statu_id' => 2],
            ['valor_causa'  => '12,200,00', 
            'assunto' => 'Mataram Ana',
            'reu' => 'CULPADASSO',
            'numero_processo' => 12, 
            'cliente_id' => 1, 
            'tipo_processo_id' => 1,
            'vara_id' => 1, 
            'statu_id' => 3],
            ]);
        }
}
