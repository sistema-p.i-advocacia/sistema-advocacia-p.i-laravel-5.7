<?php

use Illuminate\Database\Seeder;

class AdvogadoAreaAtuacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advogado_area_atuacaos')->insert([
            ['area_atuacao_id'  => 1, 'advogado_id' => 1]
        ]);
    }
}
