<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessoaFisicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoa_fisicas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data_nascimento');
            $table->string('cpf',18);
            $table->string('profissao',25)->nullable();
            $table->string('estado_civil',15);
            $table->string('escolaridade',60);
            $table->char('sexo',1);
            $table->string('etnia',20);
            $table->string('nome_mae',60);
            $table->string('nome_pai',60);
            $table->integer('cliente_id')->unsigned(); 
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoa_fisicas');
    }
}
