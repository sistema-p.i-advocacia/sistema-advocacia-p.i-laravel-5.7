<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->date('data_inicio');
            /*$table->time('hora_inicio')->default('13:00:00');;*/
            $table->date('data_termino');
            /*$table->time('hora_termino')->default('15:00:00');*/
            $table->integer('tipo_evento_id')->unsigned();
            $table->foreign('tipo_evento_id')->references('id')->on('tipo_eventos');
            $table->integer('processo_id')->nullable()->unsigned();
            $table->foreign('processo_id')->references('id')->on('processos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
