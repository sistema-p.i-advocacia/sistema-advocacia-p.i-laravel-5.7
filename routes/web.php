<?php

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

//home
Route::get('/','Site\SiteController@index');


//Advogado
Route::resource('/advogado', 'Advogado\AdvogadoController');


# /cliente
Route::resource('/cliente', 'Cliente\ClienteController');

# /evento
Route::resource('/tipo_eventos', 'Eventos\tipo\EventosController');

#calendario - D
Route::get('/eventos', 'Evento\EventoController@index')->name('eventos.index');
Route::post('/eventos', 'Evento\EventoController@novo')->name('evento.novo');
Route::post('/eventos_update', 'Evento\EventoController@update')->name('evento.update');

# /tipo_procesos
Route::resource('/tipo_processo', 'TipoProcesso\TipoProcessoController');

# /processos
Route::resource('/processo', 'Processo\ProcessoController');


# /vara
Route::resource('/vara', 'Vara\VaraController');

# /area_atuacao
Route::resource('/area_atuacao', 'AreaAtuacao\AreaAtuacaoController');

# /clientes
Route::get('/index',                  ['uses' => 'Cliente\ClienteController@index',   'as' => 'cliente.index']);
Route::get('/fisico',                 ['uses' => 'Cliente\ClienteController@createFisico','as' => 'cliente.fisicoCreate']);
Route::get('/juridico',               ['uses' => 'Cliente\ClienteController@createJuridico','as' => 'cliente.juridicoCreate']);
Route::post('/store-fisico',          ['uses' => 'Cliente\ClienteController@storeFisico',   'as' => 'cliente.fisicoStore']);
Route::post('/store-juridico',        ['uses' => 'Cliente\ClienteController@storeJuridico',   'as' => 'cliente.juridicoStore']);
Route::put('/update-fisico/{id}',     ['uses' => 'Cliente\ClienteController@updateFisico',    'as' => 'cliente.updateFisico']);
Route::put('/update-juridico/{id}',   ['uses' => 'Cliente\ClienteController@updateJuridico',    'as' => 'cliente.updateJuridico']);
Route::get('/edit-fisico/{id}',       ['uses' => 'Cliente\ClienteController@editFisico',    'as' => 'cliente.editFisico']);
Route::get('/edit-juridico/{id}',     ['uses' => 'Cliente\ClienteController@editJuridico',    'as' => 'cliente.editJuridico']);
Route::delete('/destroy-fisico/{id}', ['uses' => 'Cliente\ClienteController@destroyFisico', 'as' => 'cliente.destroyFisico']);
Route::delete('/destroy/{id}',        ['uses' => 'Cliente\ClienteController@destroyJuridico', 'as' => 'cliente.destroyJuridico']);





});







Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
