<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Processo extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'valor_causa',
        'assunto',
        'reu',
        'numero_processo',
        'cliente_id',
        'tipo_processo_id',
        'vara_id',
        'statu_id',
    ];


    public function cliente(){

        return  $this->belongsTo('App\Cliente','cliente_id');

    }
    public function tipoProcesso(){

        return  $this->belongsTo('App\TipoProcesso');

    }
    public function vara(){

        return  $this->belongsTo('App\Vara');

    }
    public function advogado(){

        return  $this->belongsTo('App\Advogado');

    }
    public function statu(){

        return  $this->belongsTo('App\Statu');

    }
  


}
