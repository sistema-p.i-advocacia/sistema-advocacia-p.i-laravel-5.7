<?php

namespace App\Http\Controllers\Eventos\tipo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TipoEvento;

class EventosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="Cadastro Tipo de Eventos";
        $eventos = TipoEvento::all();
        return view('eventos.tipo.index', ['eventos' => $eventos],compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Cadastro de Eventos";
        return view('eventos.tipo.create',compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eventos = new TipoEvento($request->all()); 
        $eventos->save();
        return redirect(route('tipo_eventos.index') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Editar Evento";
        $eventos = TipoEvento::find($id);
        return view('eventos.tipo.edit', array('eventos'=>$eventos),compact('title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $eventos = TipoEvento::find($id);
        $eventos->update($request->all());
        $eventos->save();

        return redirect(route('tipo_eventos.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventos = TipoEvento::findOrFail($id);       
        $eventos->delete();                              
        return redirect()->route('tipo_eventos.index');
    }
}
