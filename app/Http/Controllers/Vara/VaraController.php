<?php

namespace App\Http\Controllers\Vara;

use App\Vara;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="Cadastro de Vara";
        $varas = Vara::all();
        return view('vara.index', ['varas'=> $varas],compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Nova Vara";
        return view('vara.create',compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vara = new Vara($request->all());
        $vara->save();

        return redirect(route('vara.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Editar";
        $vara = Vara::find($id);
        return view('vara.edit', array('vara'=>$vara),compact('title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vara = Vara::find($id);
        $vara->update($request->all());
        $vara->save();

        return redirect(route('vara.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vara = Vara::find($id);
        $vara->delete();

        return redirect(route('vara.index'));
    }
}
