<?php

namespace App\Http\Controllers\Evento;

use App\Evento;
use App\Processo;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Calendar;
use App\TipoEvento;

class EventoController extends Controller
{
    public function index() {
        $eventos = Evento::all();
        $tipo_eventos = TipoEvento::all();
        $processos = Processo::all()->where('statu_id', '1');
        $lista_eventos = [];
        foreach ($eventos as $key => $evento) {
            $lista_eventos[] = \Calendar::event(
                $evento->nome,
                false,
                new \DateTime($evento->data_inicio),
                new \DateTime($evento->data_termino.' 23:59:59'),
                $evento->id,
                [
                    'color' => $evento->tipo_evento->cor,
                ]
            );
        }
        $calendario = Calendar::addEvents($lista_eventos )->setOptions([
            'editable' => true,
            'selectable' => true,


        ])->setCallbacks([
            'dayClick' => 'function(date, jsEvent, view) {jQuery.noConflict();
            $("#criarEvento").modal("show");
            $("#data_inicio").val(moment(date).format("YYYY-MM-DD"))
            $("#data_termino").val(moment(date).format("YYYY-MM-DD"))
            console.log(moment(date).format("YYYY-MM-DD"))}',

            /*'eventClick' => 'function(calEvent, jsEvent, view) {alert("Clickou xD evento calEvent.id")}',*/

             'eventClick' => 'function(calEvent, jsEvent, view) {jQuery.noConflict();
             $("#editarEvento").modal("show");
             $("#nomeevento").val(calEvent.title);
             
             $("#tipo_evento_id").val('.$evento->tipo_evento_id.');
             $("#evento_id").val(calEvent.id); 
             $("#processo").val('.$evento->processo_id.');
             
             console.log(calEvent); 
             
             var data_inicio = calEvent.start;
             $("#data_inicio1").val(data_inicio.format("YYYY-MM-DD"));
             var data_termino = moment(calEvent.end).format("YYYY-MM-DD");
             $("#data_termino1").val(data_termino);
             
             }',
            'eventDrop' => 'function(event) {alert("Drag n drop")}',

        
            
            ]);

        ;
        $title = 'Eventos';
        // $eventos = TipoEvento::all();
            //  dd($calendario);
        return view('eventos.eventos', compact('calendario', 'title', 'tipo_eventos', 'eventos', 'processos'));
    }

    public function novo(Request $request) {
        $validator = Validator::make($request->all(), [
            'nome' => 'required',
            'data_inicio' => 'required',
            'data_termino' => 'required',
        ]);

        if ($validator->fails()) {
            \Session::flash('warning', 'Digite informações válidas.');
            return Redirect::to('/eventos')->withInput()->withErrors($validator);
        }

        $evento = new Evento();
        $evento->nome = $request['nome'];
        $evento->data_inicio = $request['data_inicio'];
        $evento->data_termino = $request['data_termino'];
        $evento->tipo_evento_id = $request['tipo_evento'];
        $evento->processo_id = $request['processo'];
        $evento->save();

        \Session::flash('sucess', 'Evento adicionado com sucesso.');
        return Redirect::to('/eventos');
    }

    public function update(Request $request) {

        $evento = Evento::find($request['evento_id']);
        $evento->nome = $request['nomeevento'];
        $evento->data_inicio = $request['data_inicio1'];
        $evento->data_termino = $request['data_termino1'];
        $evento->tipo_evento_id = $request['tipo_evento_id'];
        $evento->processo_id = $request['processo_id'];
        $evento->save();

        return Redirect::to('/eventos');
    }
}

