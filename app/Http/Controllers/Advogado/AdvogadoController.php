<?php

namespace App\Http\Controllers\Advogado;

use App\Advogado;
use App\AdvogadoAreaAtuacao;
use App\AreaAtuacao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdvogadoRequest;



class AdvogadoController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $title ="Lista De Advogados";
        $advogados = Advogado::all();
        return view('Advogado.index', ['advogados' => $advogados],compact('title'));
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        $title ="Cadastro de Advogado";
        $AreaAtuacaos = AreaAtuacao::all();
        return view('advogado.create', ['AreaAtuacaos' => $AreaAtuacaos],compact('title'));
    }
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvogadoRequest $request)
    {
        // echo "<pre>";
        // print_r($AreaAtuacaos = ($request->area_Atuacao));die;
        // print_r($request->area_Atuacao[]);die; //pode passar a posição no array; name do input deve conter name[];
        
        $advogado = new Advogado($request->all());
        $advogado->nivel_acesso_id = 1;
        $advogado->save();  //$idAdvogado pega o id do Advogado cadastrado
    
        if($advogado->id){


            $AreaAtuacaos = ($request->area_Atuacao);

    
            for ($i=0; $i < count($AreaAtuacaos) ; $i++) { 
                $AdvogadoAreaAtuacao = new AdvogadoAreaAtuacao;   //objeto precisa ser instaciado novamente!!
                $AdvogadoAreaAtuacao->area_atuacao_id = $AreaAtuacaos[$i];
                $AdvogadoAreaAtuacao->advogado_id = $advogado->id;
                $AdvogadoAreaAtuacao->save();
            }
            
             return redirect(route('advogado.index') );

        }else{
            return redirect(route('advogado.create') );
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advogado  $advogado
     * @return \Illuminate\Http\Response
     */
    // public function show(Advogado $advogado)
    // {
    //     //
    // }
    public function show($id)
    {
        $title ="Detalhes";
        $advogado = Advogado::find($id);
        return view('advogado.show', array('advogado'=>$advogado),compact('title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advogado  $advogado
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Editar";
        $advogado = Advogado::find($id);
        return view('advogado.edit', array('advogado'=>$advogado),compact('title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advogado  $advogado
     * @return \Illuminate\Http\Response
     */

    public function update($id, AdvogadoRequest $request)
    {
        $advogado = Advogado::find($id);
        $advogado ->update($request->all());
        $advogado->save();
       
        return redirect()->route('advogado.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advogado  $advogado
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //dd($advogado);                                                //verifivar se o id advogado esta vindo.          
       $advogado = Advogado::findOrFail($id);                         // verifica se id.advogado existe no banco/ caso não exista apresenta erro 404
       $advogado->delete();                                          // detela do banco
       return redirect()->route('advogado.index');                  // return view('advogado.index'); //isso é errado tem que passar pelo metodo index da controller do advogado para fazer a listagem novamente 
    }
}
