<?php

namespace App\Http\Controllers\Processo;

use App\Processo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cliente;
use App\TipoProcesso;
use App\Vara;
use App\Advogado;
use App\Statu;
use App\AdvogadoProcesso;


class ProcessoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title=" Processos";
        $processos = Processo::all();
        // return $processos;
        return view('processo.index', ['processos'=> $processos],compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Cadastrar Processos";
        $clientes = Cliente::all();
        $tipoProcessos = TipoProcesso::all();
        $varas = Vara::all();
        $advogados = Advogado::all();
        $Status = Statu::all();
        return view('processo.create',compact('title','clientes','tipoProcessos','varas','advogados','Status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        // dd($request);
        $processo = new Processo($request->all());
        $processo->save();

        
        if($processo->id){


            $AreaAtuacaos = ($request->advogado_id);

    
            for ($i=0; $i < count($AreaAtuacaos) ; $i++) { 
                $AdvogadoProcessos = new AdvogadoProcesso;   //objeto precisa ser instaciado novamente!!
                $AdvogadoProcessos->processo_id = $processo->id;
                $AdvogadoProcessos->advogado_id = $AreaAtuacaos[$i];
                $AdvogadoProcessos->save();
            }
         
             return redirect(route('processo.index') );

        }else{
            return redirect(route('processo.create') );
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title ="Detalhes";
        $advogados = AdvogadoProcesso::where('processo_id',$id)->get();
        $processo = Processo::find($id);
        return view('processo.show', array('processo'=>$processo),compact('title','advogados'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title=" Editar Processos";
        $processos = Processo::find($id);
        $clientes = Cliente::all();
        $tipoProcessos = TipoProcesso::all();
        $varas = Vara::all();
        $advogados = Advogado::all();
        $Status = Statu::all();
        $advogadoProcessos = AdvogadoProcesso::where('processo_id',$id)->get();
        return view('processo.edit', ['processos'=> $processos],compact('title','clientes','tipoProcessos','varas','advogados','Status','advogadoProcessos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $processo = Processo::find($id);
        $processo->update($request->all());
        $processo->save();

        if($processo->id){


            $AreaAtuacaos = ($request->advogado_id);

    
            for ($i=0; $i < count($AreaAtuacaos) ; $i++) { 
                $AdvogadoProcessos = new AdvogadoProcesso;   //objeto precisa ser instaciado novamente!!
                $AdvogadoProcessos->processo_id = $processo->id;
                $AdvogadoProcessos->advogado_id = $AreaAtuacaos[$i];
                $AdvogadoProcessos->save();
            }
         
             return redirect(route('processo.index') );

        }else{
            return redirect(route('processo.create') );
        }

        return redirect(route('processo.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $processo = Processo::find($id);
        $processo->delete();

        return redirect(route('processo.index'));
    }
}
