<?php

namespace App\Http\Controllers\TipoProcesso;

use App\TipoProcesso;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipoProcessoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="Tipo Processos";
        $tipo_processos = TipoProcesso::all();
        return view('processo.tipo.index', ['tipo_processos'=> $tipo_processos],compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Cadastrar no tipo de Processos";
        return view('processo.tipo.create',compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipo_processo = new TipoProcesso($request->all());
        $tipo_processo->save();

        return redirect(route('tipo_processo.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Editar Processos";
        $tipo_processo = TipoProcesso::find($id);
        return view('processo.tipo.edit', array('tipo_processo'=>$tipo_processo),compact('title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipo_processo = TipoProcesso::find($id);
        $tipo_processo->update($request->all());
        $tipo_processo->save();

        return redirect(route('tipo_processo.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo_processo = TipoProcesso::find($id);
        $tipo_processo->delete();

        return redirect(route('tipo_processo.index'));
    }
}
