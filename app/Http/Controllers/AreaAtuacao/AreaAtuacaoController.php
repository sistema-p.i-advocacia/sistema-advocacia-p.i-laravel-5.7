<?php

namespace App\Http\Controllers\AreaAtuacao;

use App\AreaAtuacao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AreaAtuacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title ="Área de Atuação";
        $areas_atuacao = AreaAtuacao::all();

        return view('areaatuacao.index', ['areas_atuacao'=>$areas_atuacao],compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title ="Cadastro de Área de Atuação";
        return view('areaatuacao.create',compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $area_atuacao = new AreaAtuacao($request->all());
        $area_atuacao->save();

        return redirect(route('area_atuacao.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Editar Área de Atuação";
        $area_atuacao = AreaAtuacao::find($id);

        return view('areaatuacao.edit', array('area_atuacao'=>$area_atuacao),compact('title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $area_atuacao = AreaAtuacao::find($id);
        $area_atuacao->update($request->all());
        $area_atuacao->save();

        return redirect(route('area_atuacao.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area_atuacao = AreaAtuacao::find($id);
        $area_atuacao->delete();

        return redirect(route('area_atuacao.index'));
    }
}
