<?php

namespace App\Http\Controllers\Cliente;

use App\Cliente;
use App\PessoaFisica;
use App\PessoaJuridica;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClienteController extends Controller
{
    
    public function index()
    {
        $title = "Clientes";
        $clientes = PessoaFisica::all();
        $clientesJ = PessoaJuridica::all();
        // dd( $clientes);

        // foreach ($clientesJ as $key) {
        //    print_r("<pre>" . $key->cliente->nome);die;
        // }
        return view('Cliente.index', compact('clientes','title','clientesJ'));
    }

   
    public function storeFisico(Request $request)
    {
        // dd($request);
        $cliente = new Cliente($request->all());
      
        $cliente->save();
        // dd($cliente->id);   //return id
        
        $pessoaFisica = new PessoaFisica($request->all());
        $pessoaFisica->cliente_id = $cliente->id;
        $pessoaFisica->save();
        return redirect(route('cliente.index'));
    }
    public function storeJuridico(Request $request)
    {
        $cliente = new Cliente($request->all());
        $cliente->save();
        
        $pessoajuridica = new PessoaJuridica($request->all());
        $pessoajuridica->cliente_id = $cliente->id;
        $pessoajuridica->save();

        return redirect(route('cliente.index'));
    }

   
    public function createFisico()
    {
        $title = "Cadastro de Cliente Físico";
         return view('Cliente.Fisico.create',compact('title'));
    }

    public function createJuridico()
    {
        $title = "Cadastro de Cliente Júridico";
         return view('cliente.juridico.create',compact('title'));
    }

  
    public function show(Cliente $cliente)
    {
        return "create page";
    }

   
    public function editFisico($id)
    {
        $title = "Editar Pessoa Física";
        $cliente = Cliente::find($id);
        $pessoa_fisica = PessoaFisica::where('cliente_id',$id)->get()->first();
        return view('Cliente.Fisico.edit',compact('title','pessoa_fisica','cliente'));

    }
    public function editJuridico($id)
    {
        $title = "Editar Pessoa Física";
        $cliente = Cliente::find($id);
        $pessoajuridica = PessoaJuridica::where('cliente_id',$id)->get()->first();
        return view('Cliente.juridico.edit',compact('title','pessoajuridica','cliente'));
    }

    
    public function updateFisico(Request $request)
    {
    
        $cliente = Cliente::find($request->clienteID);
        $pessoa_fisica = PessoaFisica::find($request->pessoaID);
        $cliente->update($request->all());
        $cliente->save();
        $pessoa_fisica->update($request->all());
        $pessoa_fisica->save();

        return redirect(route('cliente.index'));
    }

   
    
    public function updateJuridico(Request $request)
    {
        $cliente = Cliente::find($request->clienteID);
        $pessoaJuridica = PessoaJuridica::find($request->pessoaID);
        $cliente->update($request->all());
        $cliente->save();
        $pessoaJuridica->update($request->all());
        $pessoaJuridica->save();

        return redirect(route('cliente.index'));
    }

   
    public function destroyFisico($id)
    {
      
        $cliente = Cliente::find($id);
        $pessoa = PessoaFisica::where('cliente_id',$id)->get()->first();
        $pessoa->delete();
        $cliente->delete();

        return redirect(route('cliente.index'));
    }
    public function destroyJuridico($id)
    {
        $cliente = Cliente::find($id);
        $pessoa = PessoaJuridica::where('cliente_id',$id)->get()->first();
        $pessoa->delete();
        $cliente->delete();
        return redirect(route('cliente.index'));
    }
}
