<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdvogadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //validar
        return [
            'nome' => 'required',
            'registro_oab' => 'required' ,
            'email' => 'required',
            'telefone'=> 'required' ,
        ];
    }
}
