<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PessoaFisica extends Model
{
    Use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'cpf',
        'data_nascimento',
        'estado_civil',
        'escolaridade',
        'profissao',
        'sexo',
        'etnia',
        'nome_mae',
        'nome_pai',
        'cliente_id',
        'etnia_id',

    ];


public function cliente(){

    return $this->belongsTo('App\Cliente','cliente_id');

}

}
