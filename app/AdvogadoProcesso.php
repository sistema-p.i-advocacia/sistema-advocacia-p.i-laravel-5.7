<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdvogadoProcesso extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    // protected $table = 'advogado_area_atuacao';
    protected $fillable = [
        'advogado_id',
        'processo_id',
    ];

    public function advogado(){

        return  $this->belongsTo('App\Advogado','processo_id');

    }


}
