<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PessoaJuridica extends Model
{
    Use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'cnpj',
        'data_fundacao',
        'cliente_id',

    ];

    public function cliente(){

        return $this->belongsTo('App\Cliente','cliente_id');
    
    }

}
