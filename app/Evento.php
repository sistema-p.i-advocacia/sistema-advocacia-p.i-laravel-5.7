<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $fillable = [
        'nome', 'data_inicio', 'hora_inicio', 'data_termino', 'hora_termino'
    ];

    public function tipo_evento(){
        return $this->belongsTo('App\TipoEvento');
    }
}
