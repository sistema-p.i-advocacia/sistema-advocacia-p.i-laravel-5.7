@extends('layouts.app1')

@section('conteudo')
{{ Form::open([ 'method'  => 'POST', 'route' => [ 'cliente.juridicoStore' ] ]) }}
@csrf 
<div class="container">
    <div class="row">
        <div class="col-xs-4">
            <label for="">Nome

                <input type="text" class="form-control" name="nome" placeholder="Nome " required>
            </label>
        </div>                
        <div class="col-xs-4">
            <label for="">CNPJ

                <input type="number" class="form-control" name="cnpj" placeholder="Cnpj" required>
            </label>
        </div>                
        <div class="col-xs-2">
            <label for="">Data Fundação
                <input type="date" class="form-control" name="data_fundacao" placeholder="Data Fundação" required>
            </label>
        </div>                
       
    </div> 
    <h3>Contatos</h3>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="">E-mail</label>
                <input type="email" class="form-control" name="email" placeholder="E-mail" required>
            </div>
            <div class="form-group col-md-2">
                <label for="">Telefone</label>
                <input type="text" class="form-control" name="telefone1" placeholder="Telefone" data-inputmask="&quot;mask&quot;: &quot;(99)9999-99999&quot;" data-mask=""  required>
            </div>
            <div class="form-group col-md-2">
                    <label for="">Telefone</label>
                <input type="text" class="form-control" name="telefone2" placeholder="Telefone" data-inputmask="&quot;mask&quot;: &quot;(99)9999-99999&quot;" data-mask="" >
            </div>
        </div>
        <div class=" col-xs-11 text-right">
            
            <!-- Submit -->
            <a href=" {{route('area_atuacao.index')}} " class="btn btn-info btn-sm">Voltar</a>
            {{Form::reset('Limpar', ['class'=>'btn  btn-default btn-sm'])}}
            {{Form::submit('Enviar', ['class'=>'btn btn-success btn-sm'])}}
            {{ Form::close() }}       
        </div>
    
</div> 
@endsection