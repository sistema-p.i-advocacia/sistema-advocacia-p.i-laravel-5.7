
@extends('layouts.app1')

@section('conteudo')
{{ Form::open(['method'=>'PUT', 'route'=>['cliente.updateJuridico', $cliente->id ] ]) }}
@csrf 
<div class="container">
    <div class="row">
        <div class="col-xs-4">
            <input type="hidden" name="clienteID" value=" {{ $cliente->id }} ">
            <input type="hidden" name="pessoaID"  value=" {{ $pessoajuridica->id }} " >
            <label for="">Nome

                <input type="text" value=" {{ $cliente->nome }} " class="form-control" name="nome" placeholder="Nome " required>
            </label>
        </div>                
        <div class="col-xs-4">
            <label for="">CNPJ

                <input type="text" value=" {{ $pessoajuridica->cnpj }} " class="form-control" name="cnpj" placeholder="Cnpj" required>
            </label>
        </div>                
        <div class="col-xs-2">
            <label for="">Data Fundação
                <input type="date" class="form-control" value=" {{ $pessoajuridica->data_fundacao }} " name="data_fundacao" placeholder="Data Fundação" required>
            </label>
        </div>                
       
    </div> 
    <h3>Contatos</h3>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="">E-mail</label>
                <input type="email" class="form-control" value=" {{ $cliente->email }} " name="email" placeholder="E-mail" required>
            </div>
            <div class="form-group col-md-2">
                <label for="">Telefone</label>
                <input type="text" class="form-control" value=" {{ $cliente->telefone1 }} " name="telefone1" placeholder="Telefone" data-inputmask="&quot;mask&quot;: &quot;(99)9999-99999&quot;" data-mask=""  required>
            </div>
            <div class="form-group col-md-2">
                    <label for="">Telefone</label>
                <input type="text" class="form-control" value=" {{ $cliente->telefone2 }} " name="telefone2" placeholder="Telefone" data-inputmask="&quot;mask&quot;: &quot;(99)9999-99999&quot;" data-mask="" >
            </div>
        </div>
        <div class=" col-xs-11 text-right">
            
            <!-- Submit -->
            <a href=" {{route('area_atuacao.index')}} " class="btn btn-info btn-sm">Voltar</a>
            {{Form::reset('Limpar', ['class'=>'btn  btn-default btn-sm'])}}
            {{Form::submit('Enviar', ['class'=>'btn btn-success btn-sm'])}}
            {{ Form::close() }}       
        </div>
    
</div> 
@endsection