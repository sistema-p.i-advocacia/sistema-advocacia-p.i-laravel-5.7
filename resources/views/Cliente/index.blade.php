@extends('layouts.app1')

@section('conteudo')

<a href="{{ route('cliente.fisicoCreate')  }}" class="btn  btn-info"><i class="glyphicon glyphicon-plus"></i> Pessoa Física</a>
<a href="{{ route('cliente.juridicoCreate')  }}" class="btn  btn-info"><i class="glyphicon glyphicon-plus"></i> Pessoa Jurídica</a><br><br>
<div class="col-xs-12">
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tbody><tr>
            <th>ID</th>
            <th>Clientes Fisicos</th>
            <th class="text-center">Ação</th>
          </tr>
          <tr>
            @foreach ($clientes as $cliente)
            <tr>
                <th>{{ $cliente->cliente->id }}</th>
                <td>{{$cliente->cliente->nome}}</td>
                <td class="text-center"> 
                    {{ Form::open([ 'method'  => 'DELETE', 'route' => [ 'cliente.destroyFisico', $cliente->cliente->id ], 'onsubmit' => 'return confirm("Confirmar exclusão?")' ]) }}
                    @csrf   
                

                    <a href="edit-fisico/{{ $cliente->cliente->id }} " class="btn btn-warning">Editar</a>
                    {{ Form::submit('Excluir',['class' => 'btn btn-danger']) }}
                    {{ Form::close() }}
                </td>
            </tr>                
            @endforeach
        </tbody></table>
      </div>
      <!-- /.box-body -->
      <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tbody><tr>
              <th>ID</th>
              <th>Cliente Juridicos</th>
              <th class="text-center">Ação</th>
            </tr>
            <tr>
             
              @foreach ($clientesJ as $key)
              <tr>
                  <th>{{ $key->cliente->id }}</th>
                  <td>{{$key->cliente->nome}}</td>
                  <td class="text-center"> 
                      {{ Form::open([ 'method'  => 'DELETE', 'route' => [ 'cliente.destroyJuridico', $key->cliente->id ], 'onsubmit' => 'return confirm("Confirmar exclusão?")' ]) }}
                      @csrf   
                   
  
  
                      <a href="edit-juridico/{{ $key->cliente->id }} " class="btn btn-warning">Editar</a>
                      {{ Form::submit('Excluir',['class' => 'btn btn-danger']) }}
                      {{ Form::close() }}
                  </td>
              </tr>                
              @endforeach
          </tbody></table>
        </div>
    </div>
    <!-- /.box -->


    
  </div>

  
 
@endsection