@extends('layouts.app1')
@section('conteudo')
{{-- {{dd($cliente)}} --}}
{{-- {{dd($pessoa_fisica)}} --}}

{{ Form::open(['method'=>'PUT', 'route'=>['cliente.updateFisico', $cliente->id ] ]) }}
@csrf 
<input type="hidden" value=" {{ $cliente->id }} " name="clienteID">
<input type="hidden" value=" {{ $pessoa_fisica->id }} " name="pessoaID">
<div class="container">
    <div class="row">
        <div class="col-xs-4">
            <input type="text" class="form-control" value=" {{ $cliente->nome}} " name="nome" placeholder="Nome Completo" required>
        </div>                
        <div class="col-xs-3">
            <input type="date" class="form-control" name="data_nascimento" placeholder="Data Nascimento"  required>
        </div>                
        <div class="col-xs-2">
            <input type="text" class="form-control" value=" {{ $pessoa_fisica->cpf }} "  name="cpf" placeholder="000.000.000-00" data-inputmask="&quot;mask&quot;: &quot;999.999.999-99&quot;" data-mask="" required>
        </div>     

        <div class="form-group col-md-2">
            <select class="form-control" name="estado_civil" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                <option value="">Estado Cívil </option>
                <option value="Casado">Solteiro </option>
                <option value="Casado">Casado </option>
                <option value="Separado">Separado </option>
                <option value="Divorciado">Divorciado </option>
                <option value="Viúvo">Viúvo</option>
            </select>
        </div>         
        <div class="form-group col-md-2">
            <select class="form-control" name="sexo" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                <option value="">Gênero</option>
                <option value="M">Masculino  </option>
                <option value="F">Feminino </option>
            </select>
        </div>         
        <div class="form-group col-md-2">
            <select class="form-control" name="etnia" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                <option value=" {{ $pessoa_fisica->etnia }} " > {{ $pessoa_fisica->etnia }} </option>
                <option value="Negro">Negro  </option>
                <option value="Branco">Branco </option>
                <option value="Pardo">Pardo </option>
                <option value="Amarelo">Amarelo </option>
                <option value="Indígena">Indígena </option>
            </select>
        </div>       
        <div class="form-group col-xs-3">
            <input type="text" class="form-control" value=" {{ $pessoa_fisica->profissao }} "  name="profissao" placeholder="Profissão" required>
        </div>     
        <div class="form-group col-xs-4">
            <input type="text" class="form-control" value=" {{ $pessoa_fisica->nome_mae }} "  name="nome_mae" placeholder="Nome Da Mãe" required>
        </div>     
        <div class="form-group col-xs-4">
            <input type="text" class="form-control" value=" {{ $pessoa_fisica->nome_pai }} "  name="nome_pai" placeholder="Nome Do Pai" required>
        </div>     
        <div class="form-group col-md-3">
            <select class="form-control" name="escolaridade"  style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                <option value="">Escolaridade</option>
                <option value="ensino-fundamental">Ensino fundamental</option>
                <option value="ensino-fundamental-incompleto">Ensino fundamental incompleto</option>
                <option value="ensino-medio">Ensino médio</option>
                <option value="ensino-medio-incompleto">Ensino médio incompleto</option>
                <option value="ensino-superior">Ensino superior</option>
                <option value="ensino-superior-incompleto">Ensino superior incompleto</option>
            </select>
        </div>     
    </div> 
    <h3>Contatos</h3>
        <div class="row">
            <div class="form-group col-md-4">
                <input type="email" class="form-control" value=" {{ $cliente->email}} " name="email" placeholder="E-mail" required>
            </div>
            <div class="form-group col-md-2">
                <input type="text" class="form-control" value=" {{ $cliente->telefone1}} " name="telefone1" placeholder="Telefone" data-inputmask="&quot;mask&quot;: &quot;(99)9999-99999&quot;" data-mask=""  required>
            </div>
            <div class="form-group col-md-2">
                <input type="text" class="form-control" value=" {{ $cliente->telefone2}} " name="telefone2" placeholder="Telefone" data-inputmask="&quot;mask&quot;: &quot;(99)9999-99999&quot;" data-mask="" >
            </div>
        </div>
        <div class=" col-xs-11 text-right">
            
            <!-- Submit -->
            <a href=" {{route('area_atuacao.index')}} " class="btn btn-info btn-sm">Voltar</a>
            {{Form::reset('Limpar', ['class'=>'btn  btn-default btn-sm'])}}
            {{Form::submit('Enviar', ['class'=>'btn btn-success btn-sm'])}}
            {{ Form::close() }}       
        </div>
    
</div> 

@endsection
