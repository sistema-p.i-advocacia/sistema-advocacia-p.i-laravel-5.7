{{-- Date: 2019-03-24
     By: Kelvin
--}}
@extends('layouts.app1')
@section('conteudo')

     {{ Form::open([ 'method'  => 'POST', 'route' => [ 'vara.store' ] ]) }}
     @csrf   

     <div class="row">
            <div class="col-xs-3">
                <input type="text" class="form-control" name="nome" placeholder="Nome Vara" required>
            </div>
               
            <div class="col-xs-3">
                <input type="text"  class="form-control"  name="natureza_da_vara" placeholder="Natureza da Vara" required>
            </div>
            
            <div class="col-xs-4">
                <input type="text" class="form-control" name="endereco" placeholder="Endereço" required>
            </div><br><br><br>
       </div>

    <!-- Submit -->
    <a href=" {{route('vara.index')}} " class="btn btn-info btn-sm">Voltar</a>
    {{Form::reset('Limpar', ['class'=>'btn  btn-default btn-sm'])}}
    {{Form::submit('Enviar', ['class'=>'btn btn-success btn-sm'])}}
     
    {{ Form::close() }}
        

@endsection

