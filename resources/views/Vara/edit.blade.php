@extends('layouts.app1')
@section('conteudo')


        {{-- laravel collective form --}}
        {{ Form::open(['method'=>'PUT', 'route'=>['vara.update', $vara->id ] ]) }}
        @csrf   
        
        <div class="row">
            <div class="col-xs-3">
                <input type="text" class="form-control" value=" {{$vara->nome}} " name="nome" placeholder="Nome Vara" required>
            </div>
                
            <div class="col-xs-3">
                <input type="text"  class="form-control"  value=" {{$vara->natureza_da_vara}} " name="natureza_da_vara" placeholder="Natureza da Vara" required>
            </div>
            
            <div class="col-xs-4">
                <input type="text" class="form-control" value=" {{$vara->endereco}} " name="endereco" placeholder="Endereço" required>
            </div><br><br><br>
        </div>

        <!-- Submit -->
        <a href=" {{route('advogado.index')}} " class="btn btn-info btn-sm">Voltar</a>
        {{Form::submit('Salvar', ['class'=>'btn btn-success btn-sm'])}}
        {{ Form::close() }}

    
@endsection
