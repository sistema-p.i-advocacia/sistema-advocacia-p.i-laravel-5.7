@extends('layouts.app1')
@section('conteudo')
<a href="{{ route('vara.create')  }}" class="btn  btn-info"><i class="glyphicon glyphicon-plus"></i> Nova Vara</a><br><br>



<div class="col-xs-12">
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tbody><tr>
              <th>ID</th>
              <th>Nome</th>
              <th>Natureza da Vara</th>
              <th>Endereço</th>
              <th class="text-center">Ação</th>
            </tr>
            <tr>
              @foreach ($varas as $vara)
              <tr>
                    <td>{{ $vara->id }}</td>
                    <td>{{$vara->nome}}</td>
                    <td>{{$vara->natureza_da_vara}}</td>
                    <td>{{$vara->endereco}}</td>
                  <td class="text-center"> 
                      {{ Form::open([ 'method'  => 'DELETE', 'route' => [ 'vara.destroy', $vara->id ], 'onsubmit' => 'return confirm("Confirmar exclusão?")' ]) }}
                      @csrf
                      <a href="vara/{{ $vara->id }}/edit " class="btn btn-warning ">Editar</a>
                      {{ Form::submit('Excluir',['class' => 'btn btn-danger']) }}
                      {{ Form::close() }}
                  </td>
              </tr>                
              @endforeach
          </tbody>
        {{-- </table> --}}
        </div>

@endsection

