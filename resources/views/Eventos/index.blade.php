@extends('layouts.app1')
@section('conteudo')

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-md-3">
            <div class="box box-solid">
              <div class="box-header with-border">
                <h4 class="box-title">Eventos</h4>
              </div>
              <div class="box-body">
                <!-- the events -->
                <div id="external-events">

                  
                    @foreach ($TipoEvento as $evento)
                    
                    <div class="external-event bg-light-blue"> {{ $evento->nome }} </div>
                        
                    @endforeach
                  
                </div>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="box box-primary">
              <div class="box-body no-padding">
                <!-- THE CALENDAR -->
                <div id="calendar"></div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /. box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    
    
@endsection

