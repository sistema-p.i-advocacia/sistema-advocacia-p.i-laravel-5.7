@extends('layouts.app1')
@section('conteudo')
       
        {{ Form::open([ 'method'  => 'PUT', 'route' => [ 'tipo_eventos.update', $eventos->id ] ]) }}

        @csrf   

        <div class="form-group col-xs-3">
                <label for="nome" style="vertical-align: inherit;">Nome Evento</label>
                <input value=" {{$eventos->nome}} "  type="text" class="form-control" id="nome" name="nome" placeholder="Nome Evento" required>
            </div>
        <div class="form-group col-xs-2">
                <label for="nome" style="vertical-align: inherit;">Cor Evento</label>
                <input value=" {{$eventos->cor}} " type="color" class="form-control" name="cor" required>
            </div>
        <br><br><br><br>
    
        <div class="col-xs-3" >
            <!-- Submit -->
            <a href=" {{route('tipo_eventos.index')}} " class="btn btn-info btn-sm">Voltar</a>
            {{Form::reset('Limpar', ['class'=>'btn  btn-default btn-sm'])}}
            {{Form::submit('Enviar', ['class'=>'btn btn-success btn-sm'])}}
        </div>
        {{ Form::close() }}    
 
@endsection

