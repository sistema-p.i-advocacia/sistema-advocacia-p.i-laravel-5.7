@extends('layouts.app1')
@section('conteudo')



<a href="{{ route('tipo_eventos.create')  }}" class="btn  btn-info"><i class="glyphicon glyphicon-plus"></i> Novo Evento</a>
<br><br>

<div class="col-xs-12">
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
      <table class="table table-hover">
        <tbody><tr>
            <th>ID</th>
            <th>Nome</th>
            <th>cor</th>
            <th class="text-center">Ação</th>
        </tr>
        <tr>
            @foreach ($eventos as $evento)
            <tr>
                <th>{{ $evento->id }}</th>
                <th>{{ $evento->nome }}</th>
                <td><div style="
                      width: 50px;
                      height: 20px;
                      background-color: {{$evento->cor}} ;
                    "></div></td>
                <td class="text-center"> 
                    {{ Form::open([ 'method'  => 'DELETE', 'route' => [ 'tipo_eventos.destroy', $evento->id ], 'onsubmit' => 'return confirm("Confirmar exclusão?")' ]) }}
                    @csrf
                    <a href="tipo_eventos/{{ $evento->id }}/edit " class="btn btn-warning">Editar</a>
                    {{ Form::submit('Excluir',['class' => 'btn btn-danger']) }}
                    {{ Form::close() }}
                </td>
            </tr>                
            @endforeach
      </tbody></table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>

      
@endsection