@extends('layouts.app1')
@section('conteudo')

    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/lang-all.js"></script>--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/locale/pt-br.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>





    <!-- Modal Editar -->
    <div class="modal fade" id="editarEvento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"><b>Editar evento</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                
                </div>
                    <form action="/eventos_update" method="post">
                    @csrf
                        <div class="form-group">
                            <div class="form-group">
                                <input type="hidden" id="evento_id" name="evento_id" value="">
                            </div>

                            {{--<div class="form-group">
                                <input type="hidden" id="tipo_evento_id" name="tipo_evento_id" value="2">
                            </div>--}}

                            <div class="form-group">
                                <label for="nomeevento">Evento</label>
                                <input type="text" class="form-control" name="nomeevento" id="nomeevento" aria-describedby="helpId"
                                       placeholder="">
                            </div>
                            <label for="data_inicio1">Data de início</label>
                            <input type="date"
                                   class="form-control" name="data_inicio1" id="data_inicio1" aria-describedby="helpId" placeholder="">

                        </div>
                        <div class="form-group">
                            <label for="data_termino1">Data de término</label>
                            <input type="date"
                                   class="form-control" name="data_termino1" id="data_termino1" aria-describedby="helpId" placeholder="">

                        </div>
                        <div class="form-group">
                            <select name="tipo_evento_id" id="tipo_evento_id" class="form-control select " multiple="" data-placeholder="Tipo de evento" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                @foreach ($tipo_eventos as $tipo_evento)
                                    <option value="{{ $tipo_evento->id}}" style="color: {{$tipo_evento->cor}};"> {{ $tipo_evento->nome}} </option>

                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                            <select name="processo" id="processo" class="form-control select " multiple="" data-placeholder="Tipo de evento" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                @foreach ($processos as $processo)
                                    <option value="{{ $processo->id}}"> {{ $processo->numero_processo}} - {{$processo->assunto}} </option>

                                @endforeach

                            </select>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Salvar mudança</button>
                        </div>
                        </form>
                           
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Criar -->
    <div class="modal fade" id="criarEvento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"><b>Novo evento</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(array('route' => 'evento.novo','method'=>'POST','files'=>'true')) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            @if (Session::has('success'))
                                <div class="alert alert-success">{{ Session::get('success') }}</div>
                            @elseif (Session::has('warnning'))
                                <div class="alert alert-danger">{{ Session::get('warnning') }}</div>
                            @endif

                        </div>

                        <div class="form-group">
                            {!! Form::label('nome','Nome do evento:') !!}
                            <div class="">
                                {{-- {!! Form::text('nome', null, ['class' => 'form-control']) !!} --}}
                                <input type="text" class="form-control" name="nome" required>
                                {!! $errors->first('nome', '<p class="alert alert-danger">:message</p>') !!}
                            </div>
                        </div>



                        <div class="form-group">
                            {!! Form::label('data_inicio','Data de início:') !!}
                            <div class="">
                                {{-- {!! Form::date('data_inicio', null, ['class' => 'form-control']) !!} --}}
                                <input type="date" class="form-control" id="data_inicio"  name="data_inicio" required>
                                {!! $errors->first('data_inicio', '<p class="alert alert-danger">:message</p>') !!}
                            </div>
                        </div>


                        <div class="form-group">
                            {!! Form::label('data_termino','Data de término:') !!}
                            <div class="">
                                {{-- {!! Form::date('data_termino', null, ['class' => 'form-control']) !!} --}}
                                <input type="date" class="form-control" id="data_termino" name="data_termino" required>
                                {!! $errors->first('data_termino', '<p class="alert alert-danger">:message</p>') !!}
                            </div>
                        </div>






                        <div class="form-group">
                            <label for="tipo_evento">Tipo de evento</label>
                            <select name="tipo_evento" id="tipo_evento" class="form-control select " multiple="" data-placeholder="Tipo de evento" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                @foreach ($tipo_eventos as $tipo_evento)
                                    <option value="{{ $tipo_evento->id}}" style="color: {{$tipo_evento->cor}};"> {{ $tipo_evento->nome}} </option>

                                @endforeach

                            </select>
                        </div>




                        <div class="form-group">
                            <label for="processo">Processo</label>
                            <select name="processo" id="processo" class="form-control select " multiple=""
                                    data-placeholder="Tipo de evento" style="width: 100%;" tabindex="-1"
                                    aria-hidden="true">
                                @foreach ($processos as $processo)
                                    <option value="{{ $processo->id}}"> {{ $processo->numero_processo}}
                                        - {{$processo->assunto}} </option>

                                @endforeach

                            </select>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Salvar mudança</button>
                        </div>

                    </div>

                    {!! Form::close() !!}
                </div>


            </div>

        </div>
    </div>
    </div>

        <div class="panel panel-primary">
            <div class="panel-heading">Eventos</div>
            <div class="panel-body" >
                {!! $calendario->calendar() !!}
                {!! $calendario->script() !!}
            </div>
        </div>


    <!-- Scripts -->
    <script>
        // $(document).ready(function() {
        //     $('#calendar-{{$calendario->getId()}}').fullCalendar({
        //         lang: 'pt-br'
        //     });
        // });
        // $(function() {
        //     ;
        //     $('#calendar-{{$calendario->getId()}}').fullCalendar({
        //         selectable: true,
        //         header: {
        //             left: 'prev,next today',
        //             center: 'title',
        //             right: 'month,agendaWeek,agendaDay'
        //         },
        //         dayClick: function(date) {
        //             alert('clicked ' + date.format());
        //         },
        //         select: function(startDate, endDate) {
        //             alert('selected ' + startDate.format() + ' to ' + endDate.format());
        //         }
        //     });

        // });
    </script>


{{-- data sugerida --}}
    <script>
    $(function(){
        $('#data_inicio').change(function(){
    $('#data_termino').val($(this).val());
     });
    });

    $(function(){
        $('#botao').click(function () {
            $('#teste').text("teste 123");
        });
    });



    </script>


@endsection

@section('jquery')
<script src="http://code.jquery.com/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/locale/pt-br.js"></script>
@endsection
