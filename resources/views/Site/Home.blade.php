@extends('layouts.app1')

@section('conteudo')

<?php
$Abertos = 0;
$Fechados = 0;
$Em_andamento = 0;
// echo "<pre>"; 
?>

@foreach ($processos_status as $status_processo)
{{-- {{print_r($status_processo->statu->status)}} --}}
    <?php


        if($status_processo->statu->status == 'Em andamento'){
            $Em_andamento++;
        }
        if ($status_processo->statu->status == 'Aberto') {
            $Abertos++;
        }
        if($status_processo->statu->status == 'Fechado'){
            $Fechados++;
        }

    ?>


    
@endforeach

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Abertos',     {{ $Abertos }}],
      ['Fechados',      {{ $Fechados }}],
      ['Em andamento',  {{ $Em_andamento }}]
    ]);

    var options = {
      title: 'Processos'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
  }
</script>
</head>
<body>
    
<div id="piechart" style="width: 800px; height: 400px;"></div>


</body>
    
@endsection

