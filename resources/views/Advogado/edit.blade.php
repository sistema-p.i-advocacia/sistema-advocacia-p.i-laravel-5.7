@extends('layouts.app1')
@section('conteudo')

         {{-- laravel collective form --}}
         {{ Form::open(['method'=>'PUT', 'route'=>['advogado.update', $advogado->id ] ]) }}
         @csrf   

         <div class="row">

                <div class="col-xs-3">
                    <input type="text" class="form-control" value=" {{$advogado->nome}} " name="nome" placeholder="Nome Completo" required>
                   </div>
                   
                   <div class="col-xs-3">
                       <input type="text"  class="form-control"  value=" {{$advogado->registro_oab}} " name="registro_oab" placeholder="Registro OAB" required>
                   </div>
                   
                   <div class="col-xs-3">
                       <input type="email" class="form-control" value=" {{$advogado->email}} " name="email" placeholder="E-mail" required>
                   </div> 
   
                   <div class="input-group col-xs-2">
                       <div class="input-group-addon">
                           <i class="fa fa-phone"></i>
                       </div>
                           <input type="text" class="form-control" value=" {{$advogado->telefone}} " name="telefone" data-inputmask="&quot;mask&quot;: &quot;(999) 9999-99999&quot;" data-mask=""required>
                   </div><br><br>
           </div>

         <!-- Submit -->
                <a href=" {{route('advogado.index')}} " class="btn btn-info btn-sm">Voltar</a>
                {{Form::submit('Salvar', ['class'=>'btn btn-success btn-sm'])}}
      
         {{ Form::close() }}

    
@endsection

