@extends('layouts.app1')

@section('conteudo')

<a href="{{ route('advogado.create')  }}" class="btn  btn-info"><i class="glyphicon glyphicon-plus"></i> Advogado</a><br><br>
<div class="col-xs-12">
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tbody><tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Registro OAB</th>
            <th class="text-center">Ação</th>
          </tr>
          <tr>
            @foreach ($advogados as $Advogado)
            <tr>
                <th>{{ $Advogado->id }}</th>
                <td>{{$Advogado->nome}}</td>
                <td>{{$Advogado->registro_oab}}</td>
                <td class="text-center"> 
                    {{ Form::open([ 'method'  => 'DELETE', 'route' => [ 'advogado.destroy', $Advogado->id ], 'onsubmit' => 'return confirm("Confirmar exclusão?")' ]) }}
                    @csrf   
                    <a href="advogado/{{ $Advogado->id }}" class="btn btn-primary">Ver</a>
                    <a href="advogado/{{ $Advogado->id }}/edit " class="btn btn-warning">Editar</a>
                    {{ Form::submit('Excluir',['class' => 'btn btn-danger']) }}
                    {{ Form::close() }}
                </td>
            </tr>                
            @endforeach
        </tbody></table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
 
@endsection