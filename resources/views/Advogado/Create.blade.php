{{-- Date: 2019-03-24
     By: Kelvin
--}}
@extends('layouts.app1')
@section('conteudo')
<div class="container">
    {{ Form::open([ 'method'  => 'POST', 'route' => [ 'advogado.store' ] ]) }}
    @csrf 
    <div class="container">
        <div class="row">
                <div class="col-xs-3">
                    <input type="text" class="form-control" name="nome" placeholder="Nome Completo" required>
                </div>                
                <div class="col-xs-3">
                    <input type="number"  class="form-control"  name="registro_oab" placeholder="Registro OAB" required>
                </div>
                    
                <div class="col-xs-3">
                    <input type="email" class="form-control" name="email" placeholder="E-mail" required>
                </div> 
                    
                <div class="input-group col-xs-2">
                    <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                    </div>
                        <input type="text" class="form-control" name="telefone" data-inputmask="&quot;mask&quot;: &quot;(99) 99999-9999&quot;" data-mask="">
                    </div><br>

            </div> 
            {{-- corrigir Provavelmente javascrip --}}
                <div class="input-group col-xs-6">
                    <select name="area_Atuacao[]" class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Área de atuação" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                     @foreach ($AreaAtuacaos as $AreaAtuacao)
                     
                     <option value="{{ $AreaAtuacao->id}}"> {{ $AreaAtuacao->nome}} </option>
                         
                     @endforeach
                    
                    </select>
                </div><br>
                
                

                <!-- Submit -->
                <div class="container" >
                    <div style="text-align:center"> 
                        <a href=" {{route('advogado.index')}} " class="btn btn-info btn-sm">Voltar</a>
                        {{Form::reset('Limpar', ['class'=>'btn  btn-default btn-sm'])}}
                        {{Form::submit('Enviar', ['class'=>'btn btn-success btn-sm'])}}
                        {{ Form::close() }}
                    </div>
                 </div>
    </div> 
</div>
@endsection