@extends('layouts.app1')

@section('conteudo')

<a href="{{ route('area_atuacao.create')  }}" class="btn  btn-info"><i class="glyphicon glyphicon-plus"></i> Área Atuação</a><br><br>
<div class="col-xs-12">
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tbody><tr>
            <th class="text-center" >ID</th>
            <th class="text-center" >Nome</th>
            <th class="text-center">Ação</th>
          </tr>
          <tr>
            @foreach ($areas_atuacao as $area_atuacao)
            <tr>
                <th class="text-center"  >{{ $area_atuacao->id }}</th>
                <td class="text-center">{{$area_atuacao->nome}}</td>
                <td class="text-center"> 
                    {{ Form::open([ 'method'  => 'DELETE', 'route' => [ 'area_atuacao.destroy', $area_atuacao->id ], 'onsubmit' => 'return confirm("Confirmar exclusão?")' ]) }}
                    @csrf   
                    <a href="area_atuacao/{{ $area_atuacao->id }}/edit " class="btn btn-warning">Editar</a>
                    {{ Form::submit('Excluir',['class' => 'btn btn-danger']) }}
                    {{ Form::close() }}
                </td>
            </tr>                
            @endforeach
        </tbody></table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
 
@endsection

       
          