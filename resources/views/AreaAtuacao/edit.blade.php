@extends('layouts.app1')
@section('conteudo')

        {{ Form::open(['method'=>'PUT', 'route'=>['area_atuacao.update', $area_atuacao->id ] ]) }}
        @csrf
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <input type="text" value=" {{$area_atuacao->nome}} " class="form-control" name="nome" placeholder="Área de atuação" required>
                </div>                
            </div> <br>
                    
                <!-- Submit -->
                <a href=" {{route('area_atuacao.index')}} " class="btn btn-info btn-sm">Voltar</a>
                {{Form::reset('Limpar', ['class'=>'btn  btn-default btn-sm'])}}
                {{Form::submit('Enviar', ['class'=>'btn btn-success btn-sm'])}}
                {{ Form::close() }}       
        </div> 

@endsection

