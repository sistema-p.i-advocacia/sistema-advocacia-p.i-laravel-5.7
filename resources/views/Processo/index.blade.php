@extends('layouts.app1')

@section('conteudo')
<a href="{{ route('processo.create')  }}" class="btn  btn-info"><i class="glyphicon glyphicon-plus"></i> Novo Processo</a><br><br>

{{-- {{dd($processos)}} --}}
<div class="col-xs-12">
  <!-- /.box-header -->
  <div class="box-body table-responsive no-padding">
    <table class="table table-hover">
      <tr>
        <th>ID</th>
        <th>Nome Cliente</th>
        <th>Tipo Processo</th>
        <th>Status</th>
        <th>Valor causa</th>
        <th class="text-center">Ação</th>
      </tr>
        @foreach ($processos as $processo)  
        <tr>
          <td> {{$processo->id}} </td>
          <td> {{$processo->cliente->nome}} </td>
          <td> {{$processo->tipoProcesso->nome}} </td>
          <td> {{$processo->statu->status}} </td>
          <td> {{$processo->valor_causa}} </td>
          <td class="text-center" > 
              {{ Form::open([ 'method'  => 'DELETE', 'route' => [ 'processo.destroy', $processo->id ], 'onsubmit' => 'return confirm("Confirmar exclusão?")' ]) }}
                    @csrf   
                    <a href="processo/{{ $processo->id }}" class="btn btn-primary">Ver</a>
                    <a href="processo/{{ $processo->id }}/edit " class="btn btn-warning">Editar</a>
                    {{ Form::submit('Excluir',['class' => 'btn btn-danger']) }}
              {{ Form::close() }}
          </td>
        </tr>
        
        @endforeach
        {{-- enf foresch --}}
    </tbody>

</div>
 
@endsection