{{-- Date: 2019-03-24
     By: Kelvin
--}}
@extends('layouts.app1')
@section('conteudo')

         {{-- laravel collective form --}}
         {{ Form::open([ 'method'  => 'POST', 'route' => [ 'tipo_processo.store' ] ]) }}
         @csrf   
        <div class="row">
            <div class="col-xs-3">
                <input type="text" class="form-control" name="nome" placeholder="Nome Tipo Processo" required>
            </div>
        </div><br>
            <!-- Submit -->
                <a href=" {{route('advogado.index')}} " class="btn btn-info btn-sm">Voltar</a>
                {{Form::reset('Limpar', ['class'=>'btn  btn-default btn-sm'])}}
                {{Form::submit('Enviar', ['class'=>'btn btn-success btn-sm'])}}
            
            {{ Form::close() }}
         
@endsection

