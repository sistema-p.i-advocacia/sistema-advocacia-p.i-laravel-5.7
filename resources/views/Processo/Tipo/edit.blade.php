@extends('layouts.app1')
@section('conteudo')

{{ Form::open(['method'=>'PUT', 'route'=>['tipo_processo.update', $tipo_processo->id ] ]) }}
@csrf   

    <div class="row">
            <div class="col-xs-3">
                <label for="nome">Nome do Tipo de Processo</label>
            <input id="nome" value="{{  $tipo_processo->nome }}" type="text" class="form-control" name="nome" placeholder="Nome Tipo Processo" required>
            </div>
        </div><br>

    <br>
   <!-- Submit -->
       <a href=" {{route('tipo_processo.index')}} " class="btn btn-info btn-sm">Voltar</a>
       {{Form::reset('Limpar', ['class'=>'btn  btn-default btn-sm'])}}
       {{Form::submit('Enviar', ['class'=>'btn btn-success btn-sm'])}}
   
   {{ Form::close() }}
    
@endsection

