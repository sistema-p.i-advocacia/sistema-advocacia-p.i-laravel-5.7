@extends('layouts.app1')
@section('conteudo')
<a href="{{ route('tipo_processo.create')  }}" class="btn  btn-info"><i class="glyphicon glyphicon-plus"></i> Novo Tipo de Processo</a><br><br>

          

<div class="col-xs-12">
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tbody><tr>
              <th>ID</th>
              <th>Nome</th>
              <th class="text-center">Ação</th>
            </tr>
            <tr>
              @foreach ($tipo_processos as $tipo_processo)
              <tr>
                  <th>{{ $tipo_processo->id }}</th>
                  <td>{{$tipo_processo->nome}}</td>
                  <td class="text-center"> 
                      {{ Form::open([ 'method'  => 'DELETE', 'route' => [ 'tipo_processo.destroy', $tipo_processo->id ], 'onsubmit' => 'return confirm("Confirmar exclusão?")' ]) }}
                      @csrf
                      <a href="tipo_processo/{{ $tipo_processo->id }}/edit " class="btn btn-warning">Editar</a>
                      {{ Form::submit('Excluir',['class' => 'btn btn-danger']) }}
                      {{ Form::close() }}
                  </td>
              </tr>                
              @endforeach
          </tbody>
        {{-- </table> --}}
        </div>

    
@endsection