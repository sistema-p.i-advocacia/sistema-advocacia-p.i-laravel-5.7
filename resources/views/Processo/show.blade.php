@extends('layouts.app1')
@section('conteudo')

        <div class="row text-center">
    
                <h3> <b> ID  :</b>  {{ $processo->id }} </h3>
                <h3> <b> Valor Causa : </b>${{ $processo->valor_causa }} </h3>
                <h3> <b> Assunto : </b> {{ $processo->assunto }} </h3>
                <h3> <b> Número Processo : </b>  {{ $processo->numero_processo }} </h3>
                <h3> <b> Nome Cliente : </b>  {{ $processo->cliente->nome }} </h3>
                <h3> <b> Tipo Processo : </b>  {{ $processo->tipoProcesso->nome }} </h3>
                <h3> <b> Vara : </b>  {{ $processo->vara->nome }} </h3>
                <h3> <b> Tipo Processo : </b>  {{ $processo->tipoProcesso->nome }} </h3>
                <h3> <b> Advogado(s)  : </b>  
                @foreach ($advogados as $advogado)
                    {{ $advogado->advogado->nome.":" }}
                @endforeach
                </h3>
                <h3> <b> Status : </b>  {{ $processo->statu->status }} </h3>
                
                
           
                <br>
                <a href="/processo" class="btn btn-info btn-sm">Voltar</a>
        </div>
        <br><br><br><br><br><br>
        <div class="text-center">
            
            <h1 class="">Historico</h1>

        </div>
        <a href="{{ route('processo.create')  }}" class="btn  btn-info"><i class="glyphicon glyphicon-plus"></i>add</a><br><br>

        <div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped table-bordered">
                <tr>
                <th>ID</th>
                <th>Número do Processo</th>
                <th>Tipo do Processo</th>
                <th>Nome do Cliente</th>
                <th>Advogado(s)</th>
            </tr>
            <tr>
                <th>{{ $processo->id }}</th>
                <td>{{ $processo->numero_processo }}</td>
                <td>{{ $processo->tipoProcesso->nome }}</td>
                <td>{{ $processo->cliente->nome }}</td>
                <td>
                    @foreach ($advogados as $advogado)
                        {{ $advogado->advogado->nome.":" }}
                    @endforeach 
                </td>
                </tr>
            </table>
        </div>  
            {{-- table Historico Criar tabela de historico --}}
@endsection
