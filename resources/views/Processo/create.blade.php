@extends('layouts.app1')

@section('conteudo')
{{ Form::open([ 'method'  => 'POST', 'route' => [ 'processo.store' ] ]) }}
@csrf

<div class="row">

{{-- preço input--}}
    <div class="form-group col-md-2">
        <label for="valor_causa">Preço</label>
        <input name="valor_causa" type="text" class="form-control" id="valor_causa" placeholder="Preço" required>
      </div>

      {{-- Assunto input--}}
      <div class="form-group col-md-3">
        <label for="assunto">Assunto</label>
        <input name="assunto" type="text" class="form-control" id="assunto" placeholder="Assunto" required>
      </div>
  
      {{-- reu  Select input--}}
        <div class="col-lg-3 col-sm-4" >                       
            <div class="form-group">
                <label for="exampleFormControlSelect1">Reu</label>
                <select class="form-control selectpicker bmd-label-floating" data-style="btn btn-link" id="exampleFormControlSelect1" name="reu" required>
                        <option value="">Reu</option>
                        <option value="Culpadosso"> Culpadosso </option>
                        <option value="Quem é essecara moss???"> Quem é essecara moss??? </option>
                </select>
            </div>
        </div>

        {{-- Numero Processo input--}}
        <div class="form-group col-md-2">
            <label for="numero_processo">Número Processo</label>
            <input name="numero_processo" type="text" class="form-control" id="numero_processo" placeholder="Número Processo" required>
        </div>
      
        {{-- Cliente select --}}
        <div class="col-lg-3 col-sm-4" >                       
            <div class="form-group">
                <label for="exampleFormControlSelect1">Cliente</label>
                <select class="form-control selectpicker bmd-label-floating" data-style="btn btn-link" id="exampleFormControlSelect1" name="cliente_id" required>       
                        <option value="">Selecione o cliente</option>
                        @foreach ($clientes as $cliente)
                          <option value="{{$cliente->id}}"> {{$cliente->nome}} </option>
                        @endforeach
              
                </select>
            </div>
        </div>


        {{-- Tipo Processo select--}}
        <div class="col-lg-4 col-sm-4" >                       
            <div class="form-group">
                <label for="exampleFormControlSelect1">Tipo Processo</label>
                <select class="form-control selectpicker bmd-label-floating" data-style="btn btn-link" id="exampleFormControlSelect1" name="tipo_processo_id" required>       
                        <option value="">Selecione o tipo Processo</option>
                        @foreach ($tipoProcessos as $tipoProcesso)
                          <option value="{{$tipoProcesso->id}}"> {{$tipoProcesso->nome}} </option>
                        @endforeach
              
                </select>
            </div>
        </div>

        {{-- Vara select--}}
        <div class="col-lg-3 col-sm-4" >                       
            <div class="form-group">
                <label for="exampleFormControlSelect1">Vara</label>
                <select class="form-control selectpicker bmd-label-floating" data-style="btn btn-link" id="exampleFormControlSelect1" name="vara_id" required>       
                        <option value="">Selecione a vara</option>
                        @foreach ($varas as $vara)
                          <option value="{{$vara->id}}"> {{$vara->nome}} </option>
                        @endforeach
              
                </select>
            </div>
        </div>

        {{-- advogados chips select--}}
              <div class="col-md-6">
                <div class="form-group">
                  <label>Advogado</label>
                  <select name="advogado_id[]" class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Área de atuação" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                          @foreach ($advogados as $advogado)
                          
                           <option value=" {{$advogado->id}} "> {{ $advogado->nome }} </option>
                          
                          @endforeach
                  </select>
                </div>
                <!-- /.form-group -->
              </div>

              
               {{-- Status select--}}
            <div class="col-lg-3 col-sm-4" >                       
              <div class="form-group">
                  <label for="exampleFormControlSelect1">Status</label>
                  <select class="form-control selectpicker bmd-label-floating" data-style="btn btn-link" id="exampleFormControlSelect1" name="statu_id" required>       
                    <option value="">Selecione o Status</option>
                          @foreach ($Status as $Statu)
                            <option value="{{$Statu->id}}"> {{$Statu->status}} </option>
                          @endforeach
                  </select>
              </div>
          </div>
            
          
 

</div>

<div class="col-xs-3" >
    <!-- Submit -->
    <a href=" {{route('processo.index')}} " class="btn btn-info btn-sm">Voltar</a>
    {{Form::reset('Limpar', ['class'=>'btn  btn-default btn-sm'])}}
    {{Form::submit('Enviar', ['class'=>'btn btn-success btn-sm'])}}
</div>
{{ Form::close() }}  
            
@endsection
